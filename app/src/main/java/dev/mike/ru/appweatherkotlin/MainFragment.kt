package dev.mike.ru.appweatherkotlin


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import dev.mike.ru.appweatherkotlin.mvp.presenter.MainPresenter
import dev.mike.ru.appweatherkotlin.mvp.view.MainView
import dev.mike.ru.appweatherkotlin.network.Network

class MainFragment : Fragment(), MainView {

    lateinit var presenter: MainPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_main, container, false)
        presenter = MainPresenter(Network())
        presenter.attachView(this)
        presenter.load()
        return view
    }

    override fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

}
