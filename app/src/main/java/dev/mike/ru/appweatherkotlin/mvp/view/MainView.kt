package dev.mike.ru.appweatherkotlin.mvp.view

interface MainView: View {

    fun showToast(message: String)

}