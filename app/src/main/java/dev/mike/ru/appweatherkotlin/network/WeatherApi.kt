package dev.mike.ru.appweatherkotlin.network

import dev.mike.ru.appweatherkotlin.model.WeatherExample
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("data/2.5/weather")
    fun getWeather(
            @Query("APPID") apiKey: String = "a0206d2923ac229f55ac336645680de8",
            @Query("q") cityName: String = "Nizhniy novgorod",
            @Query("units") units: String = "metric"): Single<WeatherExample>

    companion object Factory {
        fun create(): WeatherApi {
            return Retrofit.Builder()
                    .baseUrl("http://api.openweathermap.org/")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(WeatherApi::class.java)
        }
    }
}