package dev.mike.ru.appweatherkotlin.mvp.presenter

import dev.mike.ru.appweatherkotlin.mvp.view.View

interface Presenter<T : View> {
    fun attachView(view: T)
    fun detachView()
    fun load()
}