package dev.mike.ru.appweatherkotlin.network

import dev.mike.ru.appweatherkotlin.model.WeatherExample
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class Network {

    fun getWeather(): Single<WeatherExample> {
        return WeatherApi
                .create()
                .getWeather()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

}