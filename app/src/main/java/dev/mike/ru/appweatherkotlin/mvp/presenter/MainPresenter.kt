package dev.mike.ru.appweatherkotlin.mvp.presenter

import dev.mike.ru.appweatherkotlin.mvp.view.MainView
import dev.mike.ru.appweatherkotlin.network.Network
import io.reactivex.disposables.Disposable

class MainPresenter(private val network: Network) : Presenter<MainView> {

    private var view: MainView? = null
    private lateinit var disposable: Disposable

    override fun attachView(view: MainView) {
        this.view = view
    }

    override fun detachView() {
        view = null
        disposable.dispose()
    }

    override fun load() {
        disposable = network.getWeather().subscribe({weather ->
            view?.showToast(weather.name)
        }, {error ->
            view?.showToast(error.message.toString())
        })
    }
}